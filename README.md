# Booster

Booster auto replies to potential boostees with your current status.

# Why

Setting a DND message will auto reply to potential boostees, but won't answer
some of the following questions:

- Am I currently in combat?
- Where am I?
- Is my party full?

# Installation

1. Download this repository.
2. Place the extracted folder under your Addons directory.

# Example Usage

## Enabling

```
/booster on
```

## Disabling

```
/booster off
```

## Setting an auto-reply

The message specified here will be included in an auto reply which will also mention your current combat state, and how full your party/raid is.

```
/booster autoreply 12g per run
```

## Checking the current status of Booster

```
/booster status
```
