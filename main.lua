BoosterEnabled = false
BoosterAutoReply = ""

SLASH_BOOSTER1 = "/booster"
SlashCmdList["BOOSTER"] = function(command)
    if command == "" or command == "help" then
        print("/booster help: Display this help message.")
        print("/booster status: Show if booster is currently activated.")
        print("/booster on: Enable booster.")
        print("/booster off: Disable booster.")
        print("/booster autoreply <message>: Set an auto-reply message.")
        return
    elseif command == "status" then
        if BoosterEnabled then
            print("Booster is currently ENABLED with an auto reply of \"" .. BoosterAutoReply .. "\"")
        else
            print("Booster is currently DISABLED.")
        end
    elseif command == "on" then
        BoosterEnabled = true
        print("Booster has been ENABLED.")
    elseif command == "off" then
        BoosterEnabled = false
        print("Booster has been DISABLED.")
    elseif string.match(command, "autoreply") then
        BoosterAutoReply = command:gsub("autoreply ", "")
        print("Set autoreply message to: \"" .. BoosterAutoReply .. "\"")
    end
end

local frame = CreateFrame("Frame")
frame:RegisterEvent("CHAT_MSG_WHISPER")
frame:SetScript("OnEvent", function(self, event, msg, author, ...)
    if UnitGUID("player") == author then
        return
    end
    if not BoosterEnabled then
        return
    elseif BoosterAutoReply == "" then
        return print("Booster didn't autoreply, because a message hasn't been set.")
    end

    local memberCount = GetNumGroupMembers()
    local inCombat = UnitAffectingCombat("player")

    local response = BoosterAutoReply .. ". I am currently ["
    if inCombat then
        response = response .. "IN COMBAT"
    else
        response = response .. "OUT OF COMBAT"
    end
    response = response .. "]. "
    response = response .. "We are [" .. memberCount .. " / 5]. "

    SendChatMessage(response, "WHISPER", nil, author)
end)
