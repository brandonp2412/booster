## Interface: 11304
## Title: Booster
## Notes: Automatically reply to potential boostees.
## Author: brandonp2412
## Version: 1.0.0
## SavedVariablesPerCharacter: BoosterEnabled, BoosterAutoReply

main.lua